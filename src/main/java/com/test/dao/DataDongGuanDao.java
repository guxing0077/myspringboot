package com.test.dao;

import com.test.dto.DataDongGuan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/4/25.
 */
@Mapper
public interface DataDongGuanDao {
    @Select("select * from tbl_app_task_dealdata_dongguan_data")
    List<DataDongGuan> findAll();

    @Select("select * from tbl_app_task_dealdata_dongguan_data where date = #{date} and type = 2")
    DataDongGuan findOne(Map<String, Object> maps);

    @Select({"<script>","select * from tbl_app_task_dealdata_dongguan_data where date = #{date}",
            "<if test='type == null'>",
            " and type = 1",
            "</if>",
            "</script>"})
    DataDongGuan findOnes(Map<String, Object> maps);
}
