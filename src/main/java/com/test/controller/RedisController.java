package com.test.controller;

import com.test.dao.DataDongGuanDao;
import com.test.dao.DataDongGuanDao2;
import com.test.dto.DataDongGuan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/4/21.
 */
@Controller
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    private DataDongGuanDao dataDongGuanDao;

    @Autowired
    private DataDongGuanDao2 dataDongGuanDao2;

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/hello")
    @ResponseBody
    public Map<String,Object> say() {
        DataDongGuan dataDongGuan = new DataDongGuan();
        dataDongGuan.setId(1);
        dataDongGuan.setDate("2017-6-7");
        redisTemplate.opsForHash().put("mu","zi",dataDongGuan);
        Map<String,Object> maps =new HashMap<String,Object>();
        maps.put("kao",redisTemplate.opsForHash().get("mu","zi"));
        return maps;
    }

    @RequestMapping("/del")
    @ResponseBody
    public Map<String,Object> del(){
        redisTemplate.delete("mu");
        Map<String,Object> maps =new HashMap<String,Object>();
        maps.put("kao",redisTemplate.opsForHash().get("mu","zi"));
        return maps;
    }


    @RequestMapping("/findAll")
    @ResponseBody
    public List<DataDongGuan> findAll(){
        return dataDongGuanDao.findAll();
    }

    @RequestMapping("/findAll2")
    @ResponseBody
    public List<DataDongGuan> findAll2(){
        return dataDongGuanDao2.findAll();
    }

    @RequestMapping("/findOne")
    @ResponseBody
    public DataDongGuan findOne(@RequestBody Map<String,Object> maps){
        return dataDongGuanDao.findOnes(maps);
    }

    @RequestMapping("/transactional")
    @ResponseBody
    public String transactional(){
        ssss();
        return "";
    }

    @RequestMapping("/s")
    public ModelAndView show(){
        ModelAndView modelAndView = new ModelAndView("yes");
        modelAndView.addObject("host", "http://blog.didispace.com");
        return modelAndView;
    }

    @Transactional
    public void ssss(){
        int i =dataDongGuanDao2.save(new DataDongGuan(1001,2,3,4,"2017-7-20"));
    }
}
