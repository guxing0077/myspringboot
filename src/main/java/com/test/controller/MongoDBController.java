package com.test.controller;

import com.test.dao.MongoDBDao;
import com.test.dto.DataDongGuan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Administrator on 2017/4/28.
 */
@Controller
@RequestMapping("/mongodb")
public class MongoDBController {

    @Autowired
    private MongoDBDao mongoDBDao;

    @RequestMapping("/findAll")
    @ResponseBody
    public List<DataDongGuan> show(){
        return mongoDBDao.findAll();
    }

    @RequestMapping("/save")
    @ResponseBody
    public DataDongGuan save(){
        DataDongGuan taDongGuan = new DataDongGuan();
        taDongGuan.setId(2);
        taDongGuan.setDate("2017-5-6");
        taDongGuan.setHouseNum(300);
        taDongGuan.setType(1);
        return mongoDBDao.save(taDongGuan);
    }

    @RequestMapping("/findById")
    @ResponseBody
    public DataDongGuan findById(){
        return mongoDBDao.findById(1);
    }
}
